##Purpose
* This project is designed to allow users to answer survey questions pulled from a web-server. User answers are tallied on the server's database so that users can see the current voting trends.

## Testing
* An AWS hosted server instance is running at: http://ec2-52-39-179-176.us-west-2.compute.amazonaws.com/

## Server files
* server.js has the configuration code for the server
* questions.js has the interesting code covering the three endpoints necessary to grab information from the database

## Client files
* public/index.html contains all the html for the project since the requirements did not really necessitate more than one view
* public/app.js contains most of the client code. It probably could have been broken up if different states were added to the project. However, I thought that the design was simple enough not to warrant it.
* public/dataService.js manages sending requests to the server, keeping track of old answers in localStorage, and randomizing the order of questions
* public/style.css contains the css
