var databaseURI = process.env.MONGO_URI;
var collections = ["questions", "answers"];
var mongojs = require("mongojs");
var db = mongojs(databaseURI, collections);
db.toID = mongojs.ObjectId;

module.exports = db;
