var express = require('express');
var router = express.Router();
var _ = require('underscore');
var db = require('./mongoConnect');


//List the current id's of questions in the database so the client program can plan accordingly.
router.get('/list', function(req, res) {
    db.questions.find({}, function(err, list) {
        if(err) {
            return res.status(500).send("Could not compile list of questions.");
        }

        res.json(_.map(list, function(question) {
            return question._id;
        }));
    });
});

//Retrieve a specific question from the database, all the possible answers, and their corresponding tallies.
router.get('/:question', function(req, res) {
    db.questions.find({_id: db.toID(req.params.question)}, function(err, question) {
        if(err || !question || !question.length) {
            return res.status(404).send('Could not find question.');
        }

        db.answers.find({queryID: db.toID(req.params.question)}, function(err, answers) {
            if(err || !answers || !answers.length) {
                return res.status(404).send('No answers to that question.');
            }

            question[0].answers = answers;
            res.json(question[0]);
        });
    });
});

//Increase a tally by 1 for a given answer of a particular question.
router.post('/:question/answer/:answer', function(req, res) {
    db.answers.update({_id: db.toID(req.params.answer), queryID: db.toID(req.params.question)}, {$inc: {tally: 1}}, {upsert: false}, function(err, saved) {
        if(err || !saved || !saved.n) {
            return res.status(500).send("Could not increment tally.");
        }

        res.send("Increment successful.");
    });
});

function initDatabase() {
    db.questions.find({}, function(err, questions) {
        if(!err && (!questions || !questions.length)) {//Check to see if the database already has any data in it.
            db.questions.insert({text: "What is the answer to this question?"}, function(err, saved) {
                db.answers.insert([{queryID: saved._id, order: 0, text: "2", tally: 0},
                                   {queryID: saved._id, order: 1, text: "left", tally: 0},
                                   {queryID: saved._id, order: 2, text: "1539", tally: 0},
                                   {queryID: saved._id, order: 3, text: "orange", tally: 0},
                                   {queryID: saved._id, order: 4, text: "potassium", tally: 0}]);
            });
            db.questions.insert({text: "What was and/or will be the answer to the other question?"}, function(err, saved) {
                db.answers.insert([{queryID: saved._id, order: 0, text: "Whatever you want it to be.", tally: 0},
                                   {queryID: saved._id, order: 1, text: "Whatever it ends up being.", tally: 0},
                                   {queryID: saved._id, order: 2, text: "Either way, it doesn't matter.", tally: 0},
                                   {queryID: saved._id, order: 3, text: "There is more than one right answer.", tally: 0},
                                   {queryID: saved._id, order: 4, text: "There are no answers.", tally: 0}]);
            });
            db.questions.insert({text: "Does this question matter?"}, function(err, saved) {
                db.answers.insert([{queryID: saved._id, order: 0, text: "Yes.", tally: 0},
                                   {queryID: saved._id, order: 1, text: "Not at all.", tally: 0}]);
            });
        }
    });
}
initDatabase();//Populate database with example data when server starts up.

function wipeDatabase() {
    db.questions.remove({});
    db.answers.remove({});
}

module.exports = router;
