//Define a simple http server with express.
var express = require('express');
var favicon = require('express-favicon');
var app = express();
var http = require('http');
var path = require('path');
var logger = require('morgan');
var port = process.env.PORT || '3000';
app.set('port', port);
app.use(logger('tiny'));
app.use(favicon(__dirname + '/favicon.ico'));
app.set('views', __dirname);
app.set('view engine', 'ejs');

//API endpoints for survey interactivity.
var questions = require('./questions');
app.use('/question', questions);

//Serve static files.
app.use(express.static(path.join(__dirname, 'public')));

//Catch 404 and forward to error handler.
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {err: err});
});

//More configuration.
var server = http.createServer(app);
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

function onError(error) {
    if(error.syscall !== 'listen') {
        throw error;
    }

    if(error.code === 'EACCES') {
        console.error('port ' + port + ' requires elevated privileges');
        process.exit(1);
    } else if(error.code === 'EADDRINUSE') {
        console.error('port ' + port + ' is already in use');
        process.exit(1);
    } else {
        throw error;
    }
}

function onListening() {
    var addr = server.address();
    console.log('Listening on port ' + addr.port || addr);
}
