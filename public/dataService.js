(function() {
  'use strict';
  var DataService = ['$q', '$http', '$localStorage', function($q, $http, $localStorage) {
    var Data = {};
    var availableQuestions;
    var currentIndex;

    //Ask for a list of questions on the database and compare them to questions user has already answered, if any.
    //Return promise with availability of any questions.
    Data.init = function() {
      availableQuestions = [];
      return $http.get('/question/list').then(function(response) {
        var list = response.data;
        var i = list.length;
        if(i) {
          for(; i; ) {
            i-= 1;
            if(!$localStorage[list[i]]) {//Check to see if the question has already been answered.
              availableQuestions.push(list[i]);
            }
          }
          if(availableQuestions.length) {
            return true;
          }
          availableQuestions.push(list[0]);//If none of the questions are new, show the answers of an old one.
        }
        return false;
      }, $q.reject);
    };

    //Choose a random question to ask if there are any remaining questions available to the user.
    //Return promise with question and answer information.
    Data.getQuestion = function() {
      if(!availableQuestions.length) {
        return $q.reject("No questions available.");
      }
      currentIndex = Math.floor(availableQuestions.length * Math.random());
      return $http.get('/question/' + availableQuestions[currentIndex]).then(function(response) {
        response.data.myAnswer = $localStorage[availableQuestions[currentIndex]];//Include past answer, if available.
        return response.data;
      }, $q.reject);
    };

    //Send answer choice to server, triggering database tally to increment by 1.
    //Mark question as answered in local storage.
    //Return promise with availability of further questions.
    Data.sendAnswer = function(answer) {
      return $http.post('/question/' + availableQuestions[currentIndex] + '/answer/' + answer).then(function() {
        $localStorage[availableQuestions[currentIndex]] = answer;//Keep track of the user's answer for future reference.
        availableQuestions.splice(currentIndex, 1);//Remove question from the unanswered list.
        return availableQuestions.length > 0;
      }, $q.reject);
    };

    return Data;
  }];

  angular.module('Data')
    .service('Data', DataService);
}());
