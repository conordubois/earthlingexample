(function() {
  'use strict';
  var exampleCtrl = ['$scope', 'Data', '$timeout', function($scope, Data, $timeout) {
    var total = 1;
    $scope.question = "";
    $scope.answered = true;
    $scope.available = false;
    $scope.buttonText = "No questions available";
    $scope.hideMessage = true;
    $scope.hideSpinner = true;
    $scope.buttonColor = {background: "grey"};

    //When user chooses an answer, display tally while sending vote to server.
    //Update view with vote confirmation upon receipt and give a next button, if another question is available.
    $scope.submitAnswer = function() {
      if(!$scope.myAnswer) {
        return;
      }
      $scope.answered = true;
      $scope.available = false;
      $scope.buttonColor = {background: "grey"};
      $scope.buttonText = "Submitting Answer...";
      $scope.hideSpinner = false;
      Data.sendAnswer($scope.myAnswer).then(function(questionAvailable) {
        $scope.available = questionAvailable;
        if (questionAvailable) {
          $scope.buttonText = "Next Question";
          $scope.buttonColor = {background: "greenyellow"};
        } else {
          $scope.buttonText = "Done";
          $scope.buttonColor = {background: "red"};
          showMessage("There are no questions left to answer!");
        }
        $timeout(function() {
          $scope.hideSpinner = true;//Showing and hiding a spinning wheel creates more of a sense of action when answer submitted.
        }, 300);
        for(var i = 0; i < $scope.answers.length; i += 1) {
          if ($scope.answers[i]._id === $scope.myAnswer) {
            $scope.answers[i].tally += 1;//Increment local tally to match what's on the server.
            total += 1;
            break;
          }
        }
      }, function(err) {
        $scope.answered = false;
        showMessage("Error encountered while submitting answer: " + JSON.stringify(err));
        $timeout(function() {
          $scope.hideSpinner = true;
        }, 300);
      });
    };

    function showMessage(text) {//Display a message below question and answers for 5 seconds.
      $scope.warningMessage = text;
      $scope.hideMessage = false;
      $timeout(function() {
        $scope.hideMessage = true;
      }, 5000);
    }

    $scope.showPercent = function(x) {
      return Math.round(10000 * x / total) / 100;//Display percent to two decimal places.
    }

    //Show a new question to the user after clicking on the 'next' button.
    $scope.nextQuestion = function() {
      Data.getQuestion().then(function(question) {
        $scope.question = question.text;
        $scope.answers = question.answers;
        total = 0;
        for(var i = 0; i < question.answers.length; i += 1) {
          total += question.answers[i].tally;//Figure out the sum of the tallies so that percentages can be easily displayed.
        }
        if(total < 1) {
          total = 1;
        }
        if(question.myAnswer) {//If the user has already answered the question, show that answer along with tallies.
          $scope.answered = true;
          $scope.available = false;
          $scope.myAnswer = question.myAnswer;
          $scope.buttonText = "Done";
          $scope.buttonColor = {background: "red"};
          showMessage("There are no questions left to answer!");
        } else {
          $scope.answered = false;
        }
      }, handleError);
    };

    //Initialize question display.
    Data.init().then(function(questionAvailable) {
      $scope.nextQuestion();
    }, handleError);

    function handleError(err) {
      $scope.answered = true;
      $scope.available = false;
      $scope.buttonText = "Error";
      showMessage("Error encountered: " + JSON.stringify(err));
    }
  }];

  angular.module('Data', ['ngStorage']);
  angular.module('earthlingExample', ['Data'])
    .controller('ExampleController', exampleCtrl);
})();
